# -*- coding: utf-8 -*-
# settings file with live_settings options for Bistro

LIVESETTINGS_OPTIONS = {
    1: {
        'DB' : True,
        'SETTINGS': {
            'EMAIL': {
                'REPLY_BY_EMAIL': False,
                'DEFAULT_NOTIFICATION_DELIVERY_SCHEDULE_Q_ALL': 'w',
            },
            'LOGIN_PROVIDERS': {
                'PASSWORD_REGISTER_SHOW_PROVIDER_BUTTONS': False,
                'SIGNIN_AOL_ENABLED': False,
                'SIGNIN_LIVEJOURNAL_ENABLED': False,
                'SIGNIN_VERISIGN_ENABLED': False,
                'SIGNIN_IDENTI.CA_ENABLED': False,
                'SIGNIN_WORDPRESS_ENABLED': False,
                'SIGNIN_YAHOO_ENABLED': False,
                'SIGNIN_CLAIMID_ENABLED': False,
                'SIGNIN_GOOGLE_ENABLED': False,
                'SIGNIN_FACEBOOK_ENABLED': False,
                'SIGNIN_TECHNORATI_ENABLED': False,
                'SIGNIN_TWITTER_ENABLED': False,
                'SIGNIN_OPENID_ENABLED': False,
                'SIGNIN_VIDOOP_ENABLED': False,
                'SIGNIN_BLOGGER_ENABLED': False,
                'SIGNIN_LINKEDIN_ENABLED': False,
                'SIGNIN_FLICKR_ENABLED': False,
            },
            'SOCIAL_SHARING': {
                'ENABLE_SHARING_GOOGLE': False,
                'ENABLE_SHARING_TWITTER': False,
                'ENABLE_SHARING_FACEBOOK': False,
                'ENABLE_SHARING_IDENTICA': False,
                'ENABLE_SHARING_LINKEDIN': False,
            },
            'QA_SITE_SETTINGS': {
                'APP_KEYWORDS': 'au quotidien,bistro,publik',
                'APP_COPYRIGHT': 'Copyright Entr\'ouvert, 2014',
                'APP_TITLE': u'Bistro : la communauté Publik',
                'APP_DESCRIPTION': u'Le site de la communauté des utilisateurs de Publik',
                'APP_SHORT_NAME': 'Bistro',
                'GREETING_FOR_ANONYMOUS_USER': u'Première visite ?  Jetez un œil à la page de bienvenue !',
            },
            'GENERAL_SKIN_SETTINGS': {
                'ASKBOT_DEFAULT_SKIN': 'bistro',
            },
            'USER_SETTINGS': {
                'ALLOW_ADD_REMOVE_LOGIN_METHODS': False,
                'ALLOW_ANONYMOUS_FEEDBACK': False,
                'ENABLE_GRAVATAR': False,
                'NAME_OF_ANONYMOUS_USER': u'Visiteur',
                'NEW_USER_GREETING': u"""Bonjour, bienvenue sur Bistro, le site communautaire des utilisateurs de « Publik »""",
            },
            'GROUP_SETTINGS': {
                'GROUPS_ENABLED': True,
            },
            'EMAIL': {
                'ADMIN_EMAIL': 'bistro@au-quotidien.com',
                'HTML_EMAIL_ENABLED': True
            },
            'FORUM_DATA_RULES': {
                'WIKI_ON': False,
                'ALLOW_ASK_ANONYMOUSLY': False,
                'ALLOW_POSTING_BEFORE_LOGGING_IN': False,
                'TAG_LIST_FORMAT': 'cloud',
            },
            'MARKUP': {
                'MARKUP_CODE_FRIENDLY': True,
            },
            'VOTE_RULES': {
                'MIN_DAYS_FOR_STAFF_TO_ACCEPT_ANSWER': 0,
            },
            'WORDS': {},
        }
    }
}

LIVESETTINGS_OPTIONS[1]['SETTINGS']['WORDS'].update({
    'WORDS_QUESTION_PLURAL': u"contributions",
    'WORDS_QUESTION_SINGULAR': u"contribution",
    'WORDS_PLEASE_ENTER_YOUR_QUESTION': u"Titre",
    'WORDS_ASK_YOUR_QUESTION': u"Contribuer",
    'WORDS_RETAG_QUESTIONS': u"re-classe les questions",
    'WORDS_FOLLOW_QUESTIONS': u"suivre les questions",
    'WORDS_SHOW_ONLY_SELECTED_ANSWERS_TO_ENQUIRERS': u"ne montrer que la réponse sélectionnée aux demandeurs",
    'WORDS_ACCEPT_BEST_ANSWERS_FOR_YOUR_QUESTIONS': u"Accepter les meilleurs réponses pour votre question",
    'WORDS_THIS_QUESTION_IS_CURRENTLY_SHARED_ONLY_WITH': u"Phrase : cette question est actuellement partagée uniiquement avec",
    'WORDS_MARK_THIS_ANSWER_AS_CORRECT': u"marquer cette réponse comme correcte",
    'WORDS_FAVORITE_QUESTION': u"Question suivie",
    'WORDS_GAVE_AN_ANSWER': u"donner une réponse",
    'WORDS_PLEASE_FEEL_FREE_TO_ASK_YOUR_QUESTION': u"N'hésitez pas à apporter votre contribution",
    'WORDS_ASKED_FIRST_QUESTION': u"A posé la première question",
    'WORDS_EDIT_ANSWER': u"Éditer la réponse",
    'WORDS_ACCEPT_OR_UNACCEPT_THE_BEST_ANSWER': u"accepter ou refuser la meilleure réponse",
    'WORDS_ADD_ANSWER': u"Ajouter une réponse",
    'WORDS_ANSWERED_OWN_QUESTION': u"a répondu à sa propre question",
    'WORDS_INVITE_OTHERS_TO_HELP_ANSWER_THIS_QUESTION': u"proposer aux autres d'aider à répondre à cette quest  tion",
    'WORDS_ACCEPT_OR_UNACCEPT_OWN_ANSWER': u"accepter ou refuser votre propre réponse",
    'WORDS_ASK_QUESTIONS': u"pose des questions",
    'WORDS_ANSWER_OWN_QUESTIONS': u"répond à ses propres questions",
    'WORDS_UNANSWERED_QUESTION_SINGULAR': u"question non résolue",
    'WORDS_UNANSWERED_QUESTION_PLURAL': u"question résolue",
    'WORDS_DELETE_YOUR_QUESTION': u"effacer votre question",
    'WORDS_ONLY_ONE_ANSWER_PER_USER_IS_ALLOWED': u"(une seule réponse par utilisateur)",
    'WORDS_UPVOTED_ANSWERS': u"Réponses valorisées",
    'WORDS_NO_QUESTIONS_HERE': u"Pas de questions ici",
    'WORDS_GIVE_A_GOOD_ANSWER': u"donne une réponse consistante",
    'WORDS_YOUR_ANSWER': u"Votre réponse",
    'WORDS_AUTHOR_OF_THE_QUESTION': u"auteur de la question",
    'WORDS_YOU_ALREADY_GAVE_AN_ANSWER': u"vous avez déjà fourni une réponse",
    'WORDS_UPVOTED_ANSWER': u"Réponse valorisée",
    'WORDS_CLOSE_QUESTIONS': u"ferme des questions",
    'WORDS_YOU_CAN_POST_QUESTIONS_BY_EMAILING_THEM_AT': u"Vous pouvez poser vos questions en les envoyant par courriel à  ",
    'WORDS_COMMENTS_AND_ANSWERS_TO_OTHERS_QUESTIONS': u"Phrase : commente et répond aux questions des autres",
    'WORDS_REPOST_AS_A_COMMENT_UNDER_THE_OLDER_ANSWER': u"re-poster en tant que commentaire sous une réponse plus ancienne",
    'WORDS_FAMOUS_QUESTION': u"Question qui tue",
    'WORDS_ANSWER_VOTED_UP': u"Réponse avec vote positif",
    'WORDS_QUESTION_VOTED_UP': u"Question avec vote positif",
    'WORDS_QUESTION_TOOLS': u"Infos Contribution",
    'WORDS_ANSWER_SINGULAR': u"réponse",
    'WORDS_COMMUNITY_GIVES_YOU_AWARDS': u"La communauté vous récompense pour vos questions, réponses et votes",
    'WORDS_QUESTIONS_COUNTABLE_FORMS': u"question\nquestions",
    'WORDS_ANSWERS_COUNTABLE_FORMS': u"réponse\nréponses",
    'WORDS_INSTRUCTION_TO_POST_ANONYMOUSLY': u"""<span class="strong big">Vous pouvez commener à écrire anonymement</span> - votre message sera publié après que vous vous soyez connecté ou que vous ayez créé un nouveau compte.""",
    'WORDS_INSTRUCTION_TO_GIVE_ANSWERS': u"""Veuillez essayer de <strong>donner une réponse consistante</strong>, pour les discussions, <strong>utilisez les commentaires</strong> et <strong>n'oubliez pas de voter</strong>.""",
    'WORDS_INSTRUCTION_TO_ANSWER_OWN_QUESTION': u"""<span class="big strong">Vous pouvez parfaitement répondre à votre propre question</span>, mais veillez alors à donner une <strong>réponse</strong>.
Souvenez-vous que vous pouvez toujours <strong>modifier votre question originelle</strong>.""",
    'WORDS_INSTRUCTION_FOR_THE_CATEGORY_SELECTOR': u"""Classez votre question en utilisant ce sélecteur de tag ou en saisissant du texte dans la zone des tags.""",

})

