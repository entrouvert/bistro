import sys
from cStringIO import StringIO
import xml.etree.ElementTree as ET

from quixote import get_publisher

import wcs.admin.forms
import wcs.formdef
import wcs.qommon.http_request

class LocalFormDefPage(wcs.admin.forms.FormDefPage):
    def __init__(self, formdef):
        self.formdef = formdef

pub = get_publisher()
pub._set_request(wcs.qommon.http_request.HTTPRequest(None, {}))
pub.get_request().environ['SCRIPT_NAME'] = ''

xml = ET.parse(sys.argv[1]).getroot()
if [x for x in xml.getchildren() if x.tag == 'category']:
    xml.remove([x for x in xml.getchildren() if x.tag == 'category'][0])
formdef = wcs.formdef.FormDef.import_from_xml_tree(xml, charset='utf-8')
wcs_admin_page = LocalFormDefPage(formdef)
print wcs_admin_page.get_preview()
